mod jlox;

use anyhow::Result;
use std::fs;
use std::io::{self, BufRead};
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "jlox")]
struct Opt {
    /// Input file
    #[structopt(short, long, parse(from_os_str))]
    file: Option<PathBuf>,
}

/// Handle a jlox result
///
/// This will exit if a terminal error is encountered.
pub fn handle_result(result: Result<(), jlox::Error>) {
    match result {
        Ok(_) => (),
        Err(jlox::Error::ProcessingError) => eprintln!("Syntax error. Skipping."),
        Err(jlox::Error::InternalError) => {
            eprintln!("Internal error.");
            // TODO: Use a proper error code.
            std::process::exit(1);
        }
    }
}

pub fn main() -> Result<()> {
    env_logger::init();

    if let Some(file) = Opt::from_args().file {
        log::info!("Reading input form file {:#?}.", &file);
        let file_contents = fs::read_to_string(file)?;
        log::debug!("File contents: {:#?}", file_contents);

        handle_result(jlox::process(file_contents))
    } else {
        log::info!("Running in interactive mode.");

        loop {
            let stdin = io::stdin();
            for line in stdin.lock().lines() {
                handle_result(jlox::process(line.unwrap()))
            }
        }
    }

    Ok(())
}
