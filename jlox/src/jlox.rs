/// jlox errors
pub enum Error {
    /// Processing the last line ran into an error.
    ///
    /// At some point this will contain some error information. This should not terminate the
    /// prompt.
    ProcessingError,

    /// Unexpected internal error.
    ///
    /// This is a terminal condition.
    InternalError,
}

/// Process some input
pub fn process(input: String) -> Result<(), Error> {
    log::debug!("Processing input: {}", input);

    // For now, just print the input back.
    println!("{}", input);

    Ok(())
}
